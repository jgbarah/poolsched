from django.test import TestCase

from ..models import Resource, User, RGitHubToken, RGitLabToken

class TestResources(TestCase):

    def test_create(self):

        resource = Resource()
        resource.save()
        resources = Resource.objects.all()
        self.assertEqual(len(resources), 1)
        self.assertEqual(resources[0], resource)

    def test_users(self):
        """Direct and reverse relationship with users"""

        resource0 = Resource.objects.create()
        resource1 = Resource.objects.create()
        userA = User.objects.create(username='A')
        userB = User.objects.create(username='B')
        userA.resources.add(resource0, resource1)
        userB.resources.add(resource0)
        self.assertEqual(resource0.user_set.get(username='A'), userA)
        self.assertEqual(resource0.user_set.get(username='B'), userB)
        self.assertEqual(resource1.user_set.get(), userA)


class TestRGitLabToken(TestCase):

    def test_create(self):

        token = RGitLabToken()
        token.save()
        tokens = RGitLabToken.objects.all()
        self.assertEqual(len(tokens), 1)
        self.assertEqual(tokens[0], token)
        resources = Resource.objects.all()
        self.assertEqual(len(resources), 1)
        self.assertEqual(resources[0].rgitlabtoken, token)
