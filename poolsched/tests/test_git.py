from django.test import TestCase

from ..models import Intention, User
from poolsched.models import IGitRaw, IGitEnriched, RGitRepo


class TestGit(TestCase):

    def test_create(self):
        """Insert a GitRaw & GitEnriched intentions into the database"""

        repo=RGitRepo(url='https://repo.re')
        repo.save()
        intention_raw = IGitRaw(repo=repo)
        intention_raw.save()
        intention_enr = IGitEnriched(repo='repo')
        intention_enr.save()
        intentions = Intention.objects.all()
        self.assertEqual(len(intentions), 2)
        for intention in intentions:
            try:
                i = intention.igitraw
                raw = True
                self.assertEqual(intention.igitraw, intention_raw)
            except Intention.igitraw.RelatedObjectDoesNotExist:
                raw = False
            try:
                i = intention.igitenriched
                self.assertEqual(intention.igitenriched, intention_enr)
                enriched = True
            except Intention.igitenriched.RelatedObjectDoesNotExist:
                enriched = False
            if raw and enriched:
                self.assertTrue(False, "Intention is not IGitRaw nor IGitEnriched")

    def test_same_user(self):
        """Insert a GitRaw & GitEnriched intentions into the database"""

        user = User(username='pp')
        user.save()
        repo = RGitRepo(url='https://repo.re')
        repo.save()
        intention_raw = IGitRaw(repo=repo, user=user)
        intention_raw.save()
        intention_enr = IGitEnriched(repo='repo1', user=user)
        intention_enr.save()
        intentions = Intention.objects.all()
        self.assertEqual(len(intentions), 2)
        intentions = Intention.objects.filter(user=user)
        self.assertEqual(len(intentions), 2)
        intentions = Intention.objects.filter(user__username='pp')
        self.assertEqual(len(intentions), 2)
        for intention in intentions:
            try:
                i = intention.igitraw
                raw = True
                self.assertEqual(intention.igitraw, intention_raw)
            except Intention.igitraw.RelatedObjectDoesNotExist:
                raw = False
            try:
                i = intention.igitenriched
                self.assertEqual(intention.igitenriched, intention_enr)
                enriched = True
            except Intention.igitenriched.RelatedObjectDoesNotExist:
                enriched = False
            if raw and enriched:
                self.assertTrue(False, "Intention is not IGitRaw nor IGitEnriched")

    def test_user_repo(self):
        """Insert a GitRaw & GitEnriched intentions into the database"""

        users = [User(username='pp'), User(username='lo'), User(username='pi')]
        gitrepos = [RGitRepo(url='https://repo0.re'),
                    RGitRepo(url='https://repo1.re')]
        intentions = [
            IGitRaw(repo=gitrepos[0], user=users[0]),
            IGitEnriched(repo='repo1', user=users[0]),
            IGitRaw(repo=gitrepos[0], user=users[1]),
            IGitRaw(repo=gitrepos[1], user=users[1]),
            IGitRaw(repo=gitrepos[0], user=users[2])
        ]
        for user in users:
            user.save()
        for repo in gitrepos:
            repo.save()
        for intention in intentions:
            intention.save()
        idb = Intention.objects.all()
        self.assertEqual(len(idb), len(intentions))
        idb = Intention.objects.filter(user=users[0])
        self.assertEqual(len(idb), 2)
        idb = Intention.objects.filter(user__username='pp')
        self.assertEqual(len(idb), 2)
        for intention in idb:
            try:
                i = intention.igitraw
                raw = True
                self.assertEqual(intention.igitraw, intentions[0])
            except Intention.igitraw.RelatedObjectDoesNotExist:
                raw = False
            try:
                i = intention.igitenriched
                self.assertEqual(intention.igitenriched, intentions[1])
                enriched = True
            except Intention.igitenriched.RelatedObjectDoesNotExist:
                enriched = False
            if raw and enriched:
                self.assertTrue(False, "Intention is not IGitRaw nor IGitEnriched")
        idb = Intention.objects.filter(igitraw__isnull=False)
        self.assertEqual(len(idb), 4)
