from django.test import TestCase

from ..models import Worker, Job

class TestBasic(TestCase):

    def test_create(self):

        worker = Worker.objects.create(status=Worker.Status.UP)
        workers = Worker.objects.all()
        self.assertEqual(workers[0], worker)

    def test_job(self):
        """Test direct and reverse relationship with jobs"""

        worker = Worker.objects.create(status=Worker.Status.UP)
        job = Job.objects.create(status=Job.Status.WORKING,
                                 worker=worker)
        self.assertEqual(worker.job_set.all()[0], job)
