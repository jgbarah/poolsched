from django.test import TestCase

from ..models import User, Job, Intention, Worker, Resource,\
    RGitRepo, RGitHubRepo, RGitHubToken,\
    IGitRaw, IGitHubRaw, IGitHubEnriched

class TestIntentionActions(TestCase):
    """Test actions on intentions (creation)"""

    def setUp(self):

        # Create users and resources (git, GitHub repos, tokens)
        self.gitnames = ['git0.gi', 'git1.gi', 'git2.gi', 'git3.gi']
        self.githubnames = [['owner0','repo0'], ['owner0','repo1'],
                       ['owner0','repo2'], ['owner0','repo3'],
                       ['owner1','repo0'], ['owner1','repo4'],
                       ['owner2','repo0']]
        self.githubtokens = ['0123456789', '1234567890', '2345678901']
        self.usernames = ['A', 'B', 'C', 'D']
        self.gitrepos = [RGitRepo.objects.create(url='https://'+name)
                    for name in self.gitnames]
        self.githubrepos = [RGitHubRepo.objects.create(owner=name[0],
                                                       repo=name[1])
                            for name in self.githubnames]
        self.githubtokens = [RGitHubToken.objects.create(token=token)
                        for token in self.githubtokens]
        self.users = [User.objects.create(username=user) for user in self.usernames]
        # Give tokens to some users
        for i in range(len(self.githubtokens)):
            self.users[i].resources.add(self.githubtokens[i])
        # Create some jobs, add some resources for them
        self.jobs = [Job.objects.create() for job in range(5)]
        for i in range(2):
            self.jobs[i].resources.add(self.githubtokens[i], self.githubrepos[i])
        for i in range(2):
            self.jobs[i+2].resources.add(self.githubtokens[i], self.githubrepos[i+2])
        # Three intentions, for two repos
        self.intention0 = IGitHubRaw.objects.create(repo=self.githubrepos[0],
                                               user=self.users[0])
        self.intention1 = IGitHubEnriched.objects.create(repo=self.githubrepos[0],
                                                    user=self.users[0])
        self.intention2 = IGitRaw.objects.create(repo=self.gitrepos[0],
                                            user=self.users[0])
        # Workers
        self.workers = [Worker.objects.create(status=Worker.Status.UP)
                        for i in range(2)]

    def test_create_intentions(self):

        # Check all intentions
        intentions = Intention.objects.all()
        self.assertEqual(len(intentions), 3)
        # Check IGitHubRaw intentions
        intentions = Intention.objects.filter(igithubraw__isnull=False)
        self.assertEqual(len(intentions), 1)
        self.assertEqual(intentions[0].igithubraw.repo.owner, self.githubnames[0][0])
        self.assertEqual(intentions[0].igithubraw.repo.repo, self.githubnames[0][1])
        intentions = IGitHubRaw.objects.all()
        self.assertEqual(len(intentions), 1)
        self.assertEqual(intentions[0].repo.owner, self.githubnames[0][0])
        self.assertEqual(intentions[0].repo.repo, self.githubnames[0][1])
        # Check IGitHubEnriched intentions
        intentions = IGitHubEnriched.objects.all()
        self.assertEqual(len(intentions), 1)
        # Check IGitRaw intentions
        intentions = IGitRaw.objects.all()
        self.assertEqual(len(intentions), 1)

    def test_create_job(self):
        """Let's assume an intention is ready to run, create a job"""

        # Create a job, add resources to it
        job = Job.objects.create(worker=self.workers[0],
                                 status=Job.Status.WORKING)
        job.resources.add(
            self.intention0.igithubraw.repo,
            self.intention0.user.resources.get(rgithubtoken__isnull=False)
        )
        self.intention0.job = job
        self.intention0.save()
        # Check intentions with jobs
        intentions = Intention.objects.exclude(job=None)
        self.assertEqual(len(intentions), 1)
        self.assertEqual(intentions[0].job.worker, self.workers[0])
        self.assertEqual(job.intention, intentions[0].igithubraw)
        # Check that we have a GitHub token assigned to the worker (via job)
        github_tokens = Resource.objects.filter(rgithubtoken__isnull=False)
        worker = intentions[0].job.worker
        job = worker.job_set.all()[0]
        token = github_tokens.get(job=job).rgithubtoken
        self.assertEqual(token, self.githubtokens[0])
        # Check that the user for the intention and the token are the same
        self.assertEqual(intentions[0].user, token.user_set.get())

    def test_job_done(self):
        """Let's create a job for an intention, then finish it"""

        self.intention0.job = Job.objects.create(worker=self.workers[0],
                                                 status=Job.Status.WORKING)
        self.intention0.save()
        # Job done, now let's finish it. Let's get the job from the worker
        job = self.workers[0].job_set.all()[0]
        self.assertEqual(job, self.intention0.job)
        # Let's say the job is done
        job.status = Job.Status.DONE
        job.save()
        # Let's check
        self.intention0.refresh_from_db()
        self.assertEqual(self.intention0.job.status, Job.Status.DONE)