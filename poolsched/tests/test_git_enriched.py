from django.test import TestCase

from ..models import Intention
from poolsched.models import IGitEnriched


class TestGitEnriched(TestCase):

    def test_create(self):
        """Insert a single GitEnriched intention into the database"""

        intention = IGitEnriched(repo='repo1')
        intention.save()
        intentions = Intention.objects.all()
        self.assertEqual(len(intentions), 1)
        self.assertEqual(intentions[0].igitenriched, intention)
