from datetime import datetime, timezone, timedelta

from django.test import TestCase

from ..models import Intention, User, Resource, Job, IGitHubRaw, RGitHubToken, RGitHubRepo

class TestBasic(TestCase):

    def test_check(self):
        """Check if I can get resources to work in an intention"""

        user = User.objects.create(username='pp')
        repo = RGitHubRepo.objects.create(owner='owner', repo='repo')
        intention_ghr = IGitHubRaw.objects.create(repo=repo, user=user)
        token_gh = RGitHubToken.objects.create(token="1234567890")
        user.resources.add(token_gh)

        # Get intention, check it is a IGitHubRaw one
        intention = Intention.objects.all()[0]
        self.assertEqual(intention.igithubraw, intention_ghr)
        # Get GitHub tokens for the intention's user, check it is the one
        token = intention.user.resources.all()[0]
        self.assertEqual(token.rgithubtoken, token_gh)

class TestCheck(TestCase):

    def setUp(self):
        # Create user, user's intention and user's token
        user = User.objects.create(username='pp')
        self.intention_ghr = IGitHubRaw.objects.create(repo='repo1', user=user)
        self.token_gh = RGitHubToken.objects.create(token="1234567890")
        user.resources.add(self.token_gh)
        # Create jobs, add the token to their resources
        job1 = Job.objects.create()
        job2 = Job.objects.create()
        job3 = Job.objects.create()
        job1.resources.add(self.token_gh)
        job2.resources.add(self.token_gh)
        job3.resources.add(self.token_gh)

    def test_check(self):
        """Check if I can get resources to work in an intention

        This check includes getting the list of available tokens for the user"""

        # Get intention, check it is IGitHubRaw
        intention = Intention.objects.all()[0]
        self.assertEqual(intention.igithubraw, self.intention_ghr)
        # Get GitHub tokens for the intention's user, check it is the one
        token = intention.user.resources.filter(rgithubtoken__isnull=False)[0]
        self.assertEqual(token.rgithubtoken, self.token_gh)
        # Check jobs using the token
        jobs = token.job_set.all()
        self.assertEqual(len(jobs), 3)

    def test_check2(self):
        """Check if I can get resources to work in an intention

        This check includes getting the list of available tokens for the user,
        and dates, remaining rate, of those tokens.
        """

        # Get intention & token
        intention = Intention.objects.all()[0]
        token = intention.user.resources.filter(rgithubtoken__isnull=False)[0]
        # Check current available rate and reset time
        self.assertLess(token.rgithubtoken.reset, datetime.now(timezone.utc))
        self.assertEqual(token.rgithubtoken.rate, 0)

    def test_check_job(self):
        """Check if I can get resources to work in an intention, create job

        After checking, hold it by creating a new job.
        No check for unique repository for now.
        """

        # Get intention & rate
        intention = Intention.objects.all()[0]
        token = intention.user.resources.filter(rgithubtoken__isnull=False)[0]
        # Update rate and restart time
        token.rgithubtoken.reset = datetime.now(timezone.utc) + timedelta(minutes=30)
        token.rgithubtoken.rate = 5000
        token.save()
        # Get token again, check condition on it, create a new job.
        token = intention.user.resources.filter(rgithubtoken__isnull=False)[0]
        if token.rgithubtoken.reset <= datetime.now(timezone.utc) or token.rgithubtoken.rate > 0:
            # We have rate, let's create a job
            job = Job.objects.create()
            job.resources.add(token)
            intention.job = job
            intention.save()
            # Check intention
            intention = Intention.objects.all()[0]
            self.assertEqual(intention.job, job)
        else:
            self.assertFalse(True, msg="Token rate not available, but it should be avaiable")


class TestCheck(TestCase):

    def setUp(self):
        # Create user, user's intention and user's token
        user = User.objects.create(username='pp')
        self.repo = RGitHubRepo.objects.create(owner='owner', repo='repo')
        self.intention_ghr = IGitHubRaw.objects.create(repo=self.repo, user=user)
        self.token_gh = RGitHubToken.objects.create(token="1234567890")
        user.resources.add(self.token_gh)
        # Create jobs, add the token to their resources
        job1 = Job.objects.create()
        job2 = Job.objects.create()
        job3 = Job.objects.create()
        job1.resources.add(self.token_gh)
        job2.resources.add(self.token_gh)
        job3.resources.add(self.token_gh)
        job3.resources.add(self.repo)

    def test_check_repo(self):
        """Check if I can get resources to work in an intention, create job

        After checking, hold it by creating a new job.
        Include check for unique repository.
        """

        # Get intention & rate
        intention = Intention.objects.all()[0]
        token = intention.user.resources.filter(rgithubtoken__isnull=False)[0]
        # Update rate and restart time
        token.rgithubtoken.reset = datetime.now(timezone.utc) - timedelta(minutes=30)
        token.rgithubtoken.save()
        # Get token again, check condition on it, create a new job.
        token = intention.user.resources.filter(rgithubtoken__isnull=False)[0]
        if token.rgithubtoken.reset <= datetime.now(timezone.utc):
            # Now, let's check if the repo is already being worked on
            jobs = self.repo.job_set.all()
            self.assertEqual(len(jobs), 1)
            # We have rate, let's create a job
            job = Job.objects.create()
            job.resources.add(token)
            intention.job = job
            intention.save()
            # Check intention
            intention = Intention.objects.all()[0]
            self.assertEqual(intention.job, job)
        else:
            self.assertFalse(True, msg="Token still not ready, but it should be ready")
