from django.test import TestCase

from ..models import Job, Resource, Worker

class TestJobs(TestCase):

    def test_create(self):

        job = Job()
        job.save()
        jobs = Job.objects.all()
        self.assertEqual(len(jobs), 1)
        self.assertEqual(jobs[0], job)

    def test_create_n(self):

        for round in range(10):
            job = Job()
            job.save()
            jobs = Job.objects.all()
            self.assertEqual(len(jobs), round+1)
            self.assertEqual(jobs[round], job)

    def test_resources(self):
        """Direct and reverse relationship with resources"""

        job = Job()
        job.save()
        resource1 = Resource()
        resource1.save()
        resource2 = Resource()
        resource2.save()
        job.resources.add(resource1)
        jobs = Job.objects.all()
        self.assertEqual(len(jobs), 1)
        self.assertEqual(jobs[0], job)
        resource = jobs[0].resources.all()[0]
        self.assertEqual(resource.id, 1)
        linked_job = resource.job_set.all()[0]
        self.assertEqual(linked_job, job)

    def test_workers(self):

        worker = Worker.objects.create()
        for round in range(10):
            job = Job(worker=worker)
            job.save()
            jobs = Job.objects.all()
            self.assertEqual(len(jobs), round+1)
            self.assertEqual(jobs[round], job)
        for round in range(10,0,-1):
            job = Job.objects.filter(id=round)
            job.delete()
            jobs = Job.objects.all()
            self.assertEqual(len(jobs), round-1)
        assert(Worker.objects.all()[0], worker)