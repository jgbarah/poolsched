from time import time

from django.test import TestCase

from poolsched.models import RGitHubRepo, RGitHubToken, Worker, User, IGitHubEnriched, Intention, IGitHubRaw


class TestIGitHubEnriched(TestCase):
    """Test for IGitHubEnriched intentions"""

    def setUp(self):

        # Create repo, token, worker, user, assign token to user
        self.repo = RGitHubRepo.objects.create(owner='Owner',
                                               repo='Repo')
        self.token = RGitHubToken.objects.create(token='0123456789')
        self.worker = Worker.objects.create()
        self.user = User.objects.create(username='A')
        self.user.resources.add(self.token)
        # Create intention
        self.intention = IGitHubEnriched.objects.create(repo=self.repo,
                                                        user=self.user)
        self._start = time()

    def tearDown(self):
        elapsed = time() - self._start
        if elapsed > 0.5:
            print('{} ({}s)'.format(self.id(), round(elapsed, 2)))

    def test_create_previous(self):
        """Test create_previous (create previous intentions)"""

        intentions = self.intention.create_previous()
        self.assertEqual(len(intentions), 1)
        self.assertNotEqual(intentions[0].igithubraw, None)

    def test_create_deep(self):
        """Test create_deep (create intention and all previous intentions"""

        intentions = IGitHubRaw.objects.create_deep()
        self.assertEqual(len(Intention.objects.all()), 2)
        self.assertEqual(
            Intention.objects.get(igithubraw__isnull=False),
            intentions[0].intention_ptr
        )
        intentions = IGitHubEnriched.objects.create_deep()
        self.assertEqual(len(Intention.objects.all()), 4)
        self.assertEqual(
            Intention.objects.filter(igithubraw__isnull=False).get(pk=4),
            intentions[1].intention_ptr
        )
        self.assertEqual(
            Intention.objects.filter(igithubenriched__isnull=False).get(pk=3),
            intentions[0].intention_ptr
        )

    def test_create_deep_many(self):
        """Test create_deep (create intention and all previous intentions"""


        repos = [RGitHubRepo.objects.create(owner='owner', repo=reponame)
                 for reponame in [str(num) for num in range(20)]]
        for repo in repos:
            intention = IGitHubEnriched.objects.create_deep(repo=repo)