from django.test import TestCase
from django.utils.timezone import now

from ...models import Resource, RGitHubToken

class TestRGitHubToken(TestCase):
    """Test for RGitHubToken (GitHub tokens)"""

    def test_create(self):
        """Create token, check some fields"""

        RGitHubToken.objects.create(token='0123456789')
        token = RGitHubToken.objects.get()
        self.assertEqual(token.token, '0123456789')
        time_now = now()
        self.assertLess(token.reset, time_now)

        token.reset = time_now
        token.save()
        token = RGitHubToken.objects.get()
        self.assertEqual(token.reset, time_now)

    def test_create_get(self):
        """Create, and then get from the database"""

        token = RGitHubToken()
        token.save()
        # Get as RGitHubToken
        tokens = RGitHubToken.objects.all()
        self.assertEqual(len(tokens), 1)
        self.assertEqual(tokens[0], token)
        # Get as Resource
        resources = Resource.objects.all()
        self.assertEqual(len(resources), 1)
        self.assertEqual(resources[0].rgithubtoken, token)
