from datetime import timedelta
from django.test import TestCase
from django.utils.timezone import now

from ...models import Worker, User, Job, Resource, Intention,\
    RGitHubRepo, RGitHubToken, IGitHubRaw, JGitHubRaw

class TestIGitHubRaw(TestCase):
    """Test for IGitHubRaw intentions"""

    @classmethod
    def setUpTestData(cls):

        # Create repo, token, worker, user, assign token to user
        cls.repo = RGitHubRepo.objects.create(owner='Owner',
                                              repo='Repo')
        cls.token = RGitHubToken.objects.create(token='0123456789')
        cls.worker = Worker.objects.create()
        cls.user = User.objects.create(username='A')
        cls.user.resources.add(cls.token)
        # Create intention
        cls.intention = IGitHubRaw.objects.create(repo=cls.repo,
                                                  user=cls.user)

    def test_filter(self):

        intention = Intention.objects.get(igithubraw__isnull=False)
        self.assertEqual(intention.igithubraw, self.intention)

    def test_check(self):

        intention = Intention.objects.get(igithubraw__isnull=False)
        self.assertNotEqual(intention.igithubraw, None)
        intention = Intention.objects.create()
        with self.assertRaises(IGitHubRaw.DoesNotExist):
            i = intention.igithubraw


class TestIGitHubRawJob(TestCase):
    """Test for IGitHubRaw intentions combined with jobs"""

    @classmethod
    def setUpTestData(cls):

        # Create repo, token, worker, user, assign token to user
        cls.repo = RGitHubRepo.objects.create(owner='Owner',
                                              repo='Repo')
        cls.token = RGitHubToken.objects.create(token='0123456789')
        cls.worker = Worker.objects.create()
        cls.user = User.objects.create(username='A')
        cls.user.resources.add(cls.token)
        # Create intention
        cls.intention = IGitHubRaw.objects.create(repo=cls.repo,
                                                  user=cls.user)

    def test_create_job(self):
        """Create a job for a IGitHubRaw intention"""

        self.intention.create_job(self.worker)
        # Check the job is in good shape
        job = Job.objects.get()
        self.assertEqual(job.pk, 1)
        self.assertEqual(job.resources.get(rgithubtoken__isnull=False).rgithubtoken,
                         self.token)
        self.assertEqual(job.resources.get(rgithubrepo__isnull=False).rgithubrepo,
                         self.repo)

    def test_create_job_toomany(self):
        """Create a job for a IGitHubRaw intention, with too many jobs for the token"""

        # Create MAX_JOBS_TOKEN+1 repos and intentions
        repos = [RGitHubRepo.objects.create(owner='Owner',
                                            repo='Repo'+str(i))
                 for i in range(IGitHubRaw.MAX_JOBS_TOKEN+1)]
        token = RGitHubToken.objects.create(token='1234567890')
        worker = Worker.objects.create()
        user = User.objects.create(username='B')
        user.resources.add(token)
        intentions = [IGitHubRaw.objects.create(repo=repo,
                                                user=user)
                      for repo in repos]
        for intention in intentions:
            job = intention.create_job(worker)
        # Last job created should be None (too many jobs per token)
        self.assertEqual(job, None)

    def test_next_job(self):
        """Getting the next job to run works"""

        # Create some jobs (based on repos, intentions)
        repos = [RGitHubRepo.objects.create(owner='Owner',
                                              repo='Repo'+str(repo))
                 for repo in range(5)]
        intentions = [IGitHubRaw.objects.create(repo=repos[repo],
                                                user=self.user)
                      for repo in range(5)]
        for intention in intentions:
            job = intention.create_job(self.worker)
        for count in range(5,0,-1):
            self.assertEqual(count, Job.objects.count())
            job = JGitHubRaw.objects.next_job()
            job.delete()

    def test_running_job(self):
        """Test running_job"""

        # Check if there is a running job for the intention
        job = self.intention.running_job()
        self.assertEqual(job, None)
        # Create job for self.intention, a new user, a new intention (same repo)
        job = self.intention.create_job(self.worker)
        user2 = User.objects.create(username='B')
        token2 = RGitHubToken.objects.create(token='1234567890')
        user2.resources.add(token2)
        intention2 = IGitHubRaw.objects.create(repo=self.repo,
                                               user=user2)
        # Check if there is running job for the new intention
        job2 = intention2.running_job()
        self.assertEqual(job2, job)
        # Check if the job has both tokens (for both users) now
        tokens = [token.rgithubtoken for token
                  in job2.resources.filter(rgithubtoken__isnull=False)]
        self.assertIn(token2, tokens)
        self.assertIn(self.token, tokens)

class TestIGitHubRawCast(TestCase):

    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(username='A')
        cls.intention = IGitHubRaw.objects.create(
            user=user,
            status=Intention.Status.READY)

    def test_basic(self):
        """Test casting an intention of this kind."""

        casted = self.intention.cast()
        self.assertEqual(casted.__class__, IGitHubRaw)

    def test_intention(self):
        """Get a Intention, cast it as this kind."""

        intention = Intention.objects.get()
        self.assertEqual(intention.__class__, Intention)
        casted = intention.cast()
        self.assertEqual(casted.__class__, IGitHubRaw)
        casted = casted.cast()
        self.assertEqual(casted.__class__, IGitHubRaw)


class TestIMGitHubRaw(TestCase):
    """Test for IMGitHubRaw (intentions manager)"""

    @classmethod
    def setUpTestData(cls):
        """Populate the database"""

        # Some users
        cls.users = [User.objects.create(username=username)
                 for username in ['A', 'B', 'C', 'D', 'E']]
        # Five intentions done, one per user, and five tokens (two exhausted)
        for user in cls.users:
            intention = IGitHubRaw.objects.create(
                user=user,
                status=Intention.Status.DONE)
            token = RGitHubToken.objects.create(
                token=user.username + "0123456789")
            # Let's have three exhausted tokens, for users C, D, E
            if user.username in ['C', 'D', 'E']:
                token.reset = now() + timedelta(seconds=60)
                token.save()
            user.resources.add(token)
        # Three more intentions, for users A, B, C, all ready
        for user in cls.users[:3]:
            intention = IGitHubRaw.objects.create (
                user=user,
                status=Intention.Status.READY)
        # One more intention, for user A, ready
        for user in cls.users[:1]:
            intention = IGitHubRaw.objects.create (
                user=user,
                status=Intention.Status.READY)

    def test_selectable_intentions(self):

        intentions = IGitHubRaw.objects.selectable_intentions(user=self.users[0])
        self.assertEqual(len(intentions), 1)
        self.assertEqual(intentions[0].id, 6)