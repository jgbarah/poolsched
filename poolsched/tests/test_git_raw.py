from django.test import TestCase

from ..models import Intention
from poolsched.models import IGitRaw, RGitRepo


class TestGitRaw(TestCase):

    def test_create(self):
        """Insert a single GitRaw intention into the database"""

        repo = RGitRepo.objects.create(url='https://mirepo.re')
        repo = RGitRepo()
        repo.url = 'https://mirepo.re'
        repo.save()
        intention = IGitRaw(repo=repo)
        intention.save()
        intentions = Intention.objects.all()
        self.assertEqual(len(intentions), 1)
        self.assertEqual(intentions[0].igitraw, intention)
