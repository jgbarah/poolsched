"""Utility code"""

from django.utils.timezone import now

from .models import Intention, Resource, RGitHubToken

def list_intentions():
    """Utility method to list all intentions"""

    intentions = Intention.objects.all()
    list = "Now: " + str(now())
    for intention in intentions:
        user = intention.user
        token = user.resources.get(rgithubtoken__isnull=False)
        if intention.igithubraw.repo is None:
            repo = None
        else:
            repo = intention.igithubraw.repo.repo
        list = list + "Intentions: " + str(intention) + str(intention.status) + \
               ", User: " + str(user.username) + \
               ", Token: " + str(token.rgithubtoken.reset) + \
               ", Repo: " + str(repo) + "\n"
    return list

def list_tokens():
    """Utility method to list all tokens"""

    tokens = RGitHubToken.objects.all()
    list = "Tokens: (Now: " + str(now()) + ")\n"
    for token in tokens:
        list = list + str(token.id) + ", " + str(token.data()) + "\n"
    return list
