from django.db import models
from django.utils.timezone import now

from . import workers
from . import resources

class JobQuerySet(models.QuerySet):

    def str_all(self):

        jobs = self.all()
        list = ""
        for job in jobs:
            list = list + "Job: " + str(job.id) + ", " \
                + "Worker: " + str(job.worker) + ", " \
                + "Status: " + str(job.status) + ", " \
                + "Resources: " + str(job.resources) + "\n"
        return list


class Job(models.Model):

    objects = JobQuerySet().as_manager()

    class Status(models.TextChoices):
        WAITING = 'WA', "Waiting"
        WORKING = 'WO', "Working"
        DONE = 'DO', "Done"

    class StopException(Exception):
        """Raised when the job had to stop before completion.

        Usually, subclassed by the different tasks, so that
        specific arguments informing of the stop can be used.
        """

    created = models.DateTimeField(default=now, blank=True)
    worker = models.ForeignKey(workers.Worker, on_delete=models.SET_NULL,
                               default=None, null=True, blank=True)
    status = models.CharField(max_length=2, choices=Status.choices,
                              default=Status.WAITING)
    # Resources held
    resources = models.ManyToManyField(
        resources.Resource,
        default=None, blank=True
    )

class ArchJob(models.Model):
    """Archived job"""

    created = models.DateTimeField(blank=True)
    archived = models.DateTimeField(default=now, blank=True)
    worker = models.ForeignKey(workers.Worker, on_delete=models.SET_NULL,
                               default=None, null=True, blank=True)
    # Resources used
    resources = models.ManyToManyField(
        resources.Resource,
        default=None, blank=True
    )
