from random import sample

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.forms import model_to_dict

from . import resources
from .intentions import Intention

class UserManager(models.Manager):

    def all_users(self):
        return self.all()

    def random_user_ready(self, max=1):
        """Get random user ids, for users with ready Intentions.

        Ready intentions are those that are in READY status (do not have
        pending previous intentions), and still don't have a job.

        :param max: maximum number of users
        :returns:   list of User objects
        """

        q = User.objects.filter(intention__status=Intention.Status.READY,
                                intention__job=None).distinct()
        count = q.count()
        users=[q[i] for i in sample(range(count),min(max,count))]
        return users

    def data(self):
        return [user.data() for user in self]


class User(AbstractUser):

    objects = UserManager()

    resources = models.ManyToManyField(
        resources.Resource,
        default=None, blank=True
    )

    def data(self):
        """Return a dictionary with the 'true' fields

        By true fields, we mean those not expressing relations,
        identifiers, etc., that were not in the definition.
        In particular, we need to avoid the OneToOneField
        created to link to parent class, and the Id field.
        """
        fields = [field.name for field
                  in self._meta.get_fields()
                  if (not isinstance(field, models.OneToOneField) and
                     field.name != 'id')]
        return model_to_dict(self, fields=fields)
