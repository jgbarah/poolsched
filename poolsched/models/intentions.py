from logging import getLogger
from random import randint

from django.db import models
from django.forms import model_to_dict

from . import jobs

logger = getLogger(__name__)


class IntentionManager(models.Manager):

    def create_deep(self, **kwargs):
        """Create, recursively, all previous intentions"""

        intention = self.create(**kwargs)
        intentions = intention.create_previous()
        return [intention] + intentions

    def data(self):
        return [intention.data() for intention in self]


class Intention(models.Model):
    """Intention: Somethig you want to achieve

    Intentions are states you want to achieve, such as "raw index collected",
    or "eniriched index built".
    """

    class Meta:
        abstract = False
    objects = IntentionManager()

    class Status(models.TextChoices):
        WAITING = 'WA', "Waiting" # Waiting for previous intentions
        READY = 'RE', "Ready" # All previous intentions done
        WORKING = 'WO', "Working" # Some job working for this intention
        DONE = 'DO', "Done" # This intention is done

    job = models.OneToOneField(jobs.Job, on_delete=models.SET_NULL,
                               default=None, null=True, blank=True)
    user = models.ForeignKey('User', on_delete=models.PROTECT,
                             default=None, null=True, blank=True)
    status = models.CharField(max_length=2, choices=Status.choices,
                              default=Status.WAITING)
    # Directly previous intentions (need to be done before this can be done)
    previous = models.ManyToManyField(
        'self',
        default=None, blank=True,
        symmetrical=False
    )

    def create_previous(self):
        "Create all needed previous intentions (no previous intention needed)"
        return []

    def data(self):
        """Return a dictionary with the 'true' fields

        By true fields, we mean those not expressing relations,
        identifiers, etc., that were not in the definition.
        In particular, we need to avoid the OneToOneField
        created by child classes, and the Id field.
        """
        fields = [field.name for field
                  in self._meta.get_fields()
                  if (not isinstance(field, models.OneToOneField) and
                     field.name != 'id')]
        return model_to_dict(self, fields=fields)

    _subfields_list = None

    @classmethod
    def _subfields(cls):
        """Get all fields corresponding to child classes

        We only run this the first time it is actually called"""

        if cls._subfields_list is None:
            cls._subfields_list = \
                [child._meta.model_name for child in cls.__subclasses__()]
        return cls._subfields_list

    def cast(self):
        """Cast to the children, if any

        Based on https://stackoverflow.com/a/22302235/2075265

        :return: children model, or self, if it is a child
        """
        for field in self._subfields():
            try:
                attr = getattr(self, field)
            except:
                # Some subfield is not an attribute, check the rest
                pass
            else:
                # Child attribute found
                return attr
        # Exception raised, or all subfield attributes are None
        logger.debug(f"Casting as intention (error?): {self}, {self.__class__}")
        return self
