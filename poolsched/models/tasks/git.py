from django.db import models

from ..intentions import Intention
from ..resources import Resource


class RGitRepo(Resource):

    url = models.URLField()


class IGitRaw(Intention):

    repo = models.ForeignKey(RGitRepo, on_delete=models.PROTECT,
                             default=None, null=True, blank=True)


class IGitEnriched(Intention):

    repo = models.CharField(max_length=128)
