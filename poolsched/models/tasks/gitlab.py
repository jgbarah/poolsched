from django.db import models

from ..intentions import Intention
from ..resources import Resource

class RGitLabRepo(Resource):

    owner = models.CharField(max_length=40)
    repo = models.CharField(max_length=40)


class RGitLabToken(Resource):

    # Token string
    token = models.CharField(max_length=40)
    # Rate limit remaining, last time it was checked
    #rate = models.IntegerField(default=0)
    # Rate limit reset, last time it was checked
    reset = models.DateTimeField(auto_now=True)


class IGitLabRaw(Intention):

    repo = models.ForeignKey(RGitLabRepo, on_delete=models.PROTECT,
                             default=None, null=True, blank=True)
