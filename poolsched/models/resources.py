from django.db import models
from django.forms import model_to_dict

class Resource(models.Model):

    class Meta:
        abstract = False

    def data(self):
        """Return a dictionary with data (not relationship) fields"""

        fields = [field.name for field
                  in self._meta.get_fields()
                  if (not isinstance(field, models.OneToOneField) and
                      not isinstance(field, models.ManyToManyField) and
                      not isinstance(field, models.ForeignKey))]
        return model_to_dict(self, fields=fields)

    def fields(self):
        """Return a dictionary with all fields"""

        fields = [field.name for field in self._meta.get_fields(include_hidden=True)]
        return model_to_dict(self, fields=fields)
